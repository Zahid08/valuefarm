<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "batchcontent".
 *
 * @property int $batchcontent_id
 * @property string|null $description
 * @property string $coursecontent_id
 * @property string|null $start_time
 * @property string|null $end_time
 * @property string|null $video_link
 * @property int|null $active
 * @property int $seq
 * @property int $batch_id
 *
 * @property Batch $batch
 */
class Batchcontent extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'batchcontent';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['coursecontent_id', 'seq', 'batch_id'], 'required'],
            [['start_time', 'end_time'], 'safe'],
            [['active', 'seq', 'batch_id'], 'integer'],
            [['description'], 'string', 'max' => 500],
            [['coursecontent_id'], 'string', 'max' => 80],
            [['video_link'], 'string', 'max' => 255],
            [['batch_id', 'seq'], 'unique', 'targetAttribute' => ['batch_id', 'seq']],
            [['batch_id'], 'exist', 'skipOnError' => true, 'targetClass' => Batch::className(), 'targetAttribute' => ['batch_id' => 'batch_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'batchcontent_id' => 'Batchcontent ID',
            'description' => 'Description',
            'coursecontent_id' => 'Coursecontent_id',
            'start_time' => 'Start Time',
            'end_time' => 'End Time',
            'video_link' => 'Video Link',
            'active' => 'Active',
            'seq' => 'Seq',
            'batch_id' => 'Batch ID',
        ];
    }

    /**
     * Gets query for [[Batch]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBatch()
    {
        return $this->hasOne(Batch::className(), ['batch_id' => 'batch_id']);
    }

    public function getBatchName()
    {
        $c = $this->batch;
        return !$c ? '' : $this->batch->batch_name;
    }

    public function getStartDateText()
    {
        $date = strtotime($this->start_time);
        return !$this->start_time ? Yii::$app->formatter->nullDisplay : 
            (date("H:i:s",$date) == '00:00:00' ? date("Y-m-d",$date) : date("Y-m-d H:i:s",$date));
    }

    public function getEndDateText()
    {
        $date = strtotime($this->end_time);
        return !$this->end_time ? Yii::$app->formatter->nullDisplay : 
            (date("H:i:s",$date) == '00:00:00' ? date("Y-m-d",$date) : date("Y-m-d H:i:s",$date));
    }
}
