<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "package".
 *
 * @property string $package_id
 * @property string|null $description
 * @property string|null $renewal
 * @property int|null $auto_approve
 *
 * @property Packagecontent[] $packagecontents
 * @property Paymentpackage[] $paymentpackages
 * @property Paymentproduct[] $paymentproducts
 * @property Product[] $products
 * @property Renewal[] $renewals
 * @property Users[] $users
 * @property Users[] $users0
 * @property Users[] $users1
 */
class Package extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'package';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['package_id'], 'required'],
            [['auto_approve'], 'integer'],
            [['package_id'], 'string', 'max' => 30],
            [['description'], 'string', 'max' => 80],
            [['renewal'], 'string', 'max' => 255],
            [['package_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'package_id' => 'Package ID',
            'description' => 'Description',
            'renewal' => 'Renewal',
            'auto_approve' => 'Auto Approve',
        ];
    }

    /**
     * Gets query for [[Packagecontents]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPackagecontents()
    {
        return $this->hasMany(Packagecontent::className(), ['package_id' => 'package_id']);
    }

    /**
     * Gets query for [[Paymentpackages]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentpackages()
    {
        return $this->hasMany(Paymentpackage::className(), ['package' => 'package_id']);
    }

    /**
     * Gets query for [[Paymentproducts]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentproducts()
    {
        return $this->hasMany(Paymentproduct::className(), ['package' => 'package_id']);
    }

    /**
     * Gets query for [[Products]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['package' => 'package_id']);
    }

    /**
     * Gets query for [[Renewals]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRenewals()
    {
        return $this->hasMany(Renewal::className(), ['package' => 'package_id']);
    }

    /**
     * Gets query for [[Users]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(Users::className(), ['Package' => 'package_id']);
    }

    /**
     * Gets query for [[Users0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsers0()
    {
        return $this->hasMany(Users::className(), ['Package2' => 'package_id']);
    }

    /**
     * Gets query for [[Users1]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsers1()
    {
        return $this->hasMany(Users::className(), ['Package3' => 'package_id']);
    }
}
