<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "batch".
 *
 * @property int $batch_id
 * @property string|null $batch_name
 * @property int|null $batch_no
 * @property string|null $date
 * @property int|null $active
 * @property string|null $description
 * @property int $course_id
 * @property string $lastupdate
 *
 * @property Course $course
 * @property Batchcontent[] $batchcontents
 * @property Userbatch[] $userbatches
 */
class Batch extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'batch';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['batch_no', 'active', 'course_id'], 'integer'],
            [['date', 'lastupdate','package','expiry','assign_users'], 'safe'],
            [['course_id','batch_name','batch_no'], 'required'],
            [['batch_name'], 'string', 'max' => 80],
            [['description'], 'string', 'max' => 500],
            [['batch_name', 'batch_no'], 'unique', 'targetAttribute' => ['batch_name', 'batch_no']],
            [['course_id'], 'exist', 'skipOnError' => true, 'targetClass' => Course::className(), 'targetAttribute' => ['course_id' => 'course_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'batch_id' => 'Batch ID',
            'assign_users' => 'Assign User',
            'batch_name' => 'Batch Name',
            'batch_no' => 'Batch No',
            'date' => 'Start Date',
            'active' => 'Active',
            'description' => 'Description',
            'course_id' => 'Course ID',
            'package' => 'Package',
            'expiry' => 'Expire Date',
        ];
    }

    /**
     * Gets query for [[Course]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCourse()
    {
        return $this->hasOne(Course::className(), ['course_id' => 'course_id']);
    }
    
    /**
     * Gets query for [[Batchcontents]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBatchcontents()
    {
        return $this->hasMany(Batchcontent::className(), ['batch_id' => 'batch_id']);
    }

    /**
     * Gets query for [[Userbatches]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUserbatches()
    {
        return $this->hasMany(Userbatch::className(), ['batch_id' => 'batch_id']);
    }
    
}
