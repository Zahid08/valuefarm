<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Users;

/**
 * UsersSearch represents the model behind the search form of `app\models\Users`.
 */
class UsersSearch extends Users
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['UserID', 'UserID2', 'UserPWD', 'UserKey', 'UserName', 'UserExpiryDate', 'pre_mco_expiry', 'pre_mco_expiry_vf', 'last_expiry', 'UserLastUpdatedDate', 'UserNextRegistrationDate', 'SerialKey', 'SerialKeyM', 'SerialKeyV', 'referral_code', 'agent_id', 'LastLogin', 'Device', 'SessionID', 'LastLogin2', 'Device2', 'SessionID2', 'WebSession', 'Email', 'Phone', 'IC', 'Address', 'Country', 'Package', 'Package2', 'expiry2', 'last_expiry2', 'Package3', 'expiry3', 'Versions', 'DeviceToken', 'dob', 'gender', 'profile_update', 'create_date', 'self_create_date', 'payment', 'payment2', 'remark', 'auth_key'], 'safe'],
            [['UserRole', 'Admin', 'Build', 'age_group', 'invest_exp', 'special1', 'special2', 'special3', 'years1', 'years2', 'years3', 'sector1', 'sector2', 'sector3', 'sector4', 'hide_profile', 'profile_status', 'mtp_group'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$assignUserData='',$expireDate='')
    {
        $query = Users::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pagesize' => Yii::$app->session->get('usersPageSize',Yii::$app->params['listPerPage']),
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'UserExpiryDate' => $this->UserExpiryDate,
            'pre_mco_expiry' => $this->pre_mco_expiry,
            'pre_mco_expiry_vf' => $this->pre_mco_expiry_vf,
            'last_expiry' => $this->last_expiry,
            'UserLastUpdatedDate' => $this->UserLastUpdatedDate,
            'UserNextRegistrationDate' => $this->UserNextRegistrationDate,
            'UserRole' => $this->UserRole,
            'Admin' => $this->Admin,
            'LastLogin' => $this->LastLogin,
            'LastLogin2' => $this->LastLogin2,
            'WebSession' => $this->WebSession,
            'expiry2' => $this->expiry2,
            'last_expiry2' => $this->last_expiry2,
            'expiry3' => $this->expiry3,
            'Build' => $this->Build,
            'dob' => $this->dob,
            'age_group' => $this->age_group,
            'invest_exp' => $this->invest_exp,
            'special1' => $this->special1,
            'special2' => $this->special2,
            'special3' => $this->special3,
            'years1' => $this->years1,
            'years2' => $this->years2,
            'years3' => $this->years3,
            'sector1' => $this->sector1,
            'sector2' => $this->sector2,
            'sector3' => $this->sector3,
            'sector4' => $this->sector4,
            'hide_profile' => $this->hide_profile,
            'profile_update' => $this->profile_update,
            'profile_status' => $this->profile_status,
            'create_date' => $this->create_date,
            'self_create_date' => $this->self_create_date,
            'mtp_group' => $this->mtp_group,
        ]);

        $query->andFilterWhere(['like', 'UserID', $this->UserID])
            ->andFilterWhere(['like', 'UserID2', $this->UserID2])
            ->andFilterWhere(['like', 'UserPWD', $this->UserPWD])
            ->andFilterWhere(['like', 'UserKey', $this->UserKey])
            ->andFilterWhere(['like', 'UserName', $this->UserName])
            ->andFilterWhere(['like', 'SerialKey', $this->SerialKey])
            ->andFilterWhere(['like', 'SerialKeyM', $this->SerialKeyM])
            ->andFilterWhere(['like', 'SerialKeyV', $this->SerialKeyV])
            ->andFilterWhere(['like', 'referral_code', $this->referral_code])
            ->andFilterWhere(['like', 'agent_id', $this->agent_id])
            ->andFilterWhere(['like', 'Device', $this->Device])
            ->andFilterWhere(['like', 'SessionID', $this->SessionID])
            ->andFilterWhere(['like', 'Device2', $this->Device2])
            ->andFilterWhere(['like', 'SessionID2', $this->SessionID2])
            ->andFilterWhere(['like', 'Email', $this->Email])
            ->andFilterWhere(['like', 'Phone', $this->Phone])
            ->andFilterWhere(['like', 'IC', $this->IC])
            ->andFilterWhere(['like', 'Address', $this->Address])
            ->andFilterWhere(['like', 'Country', $this->Country])
            ->andFilterWhere(['like', 'Package', $this->Package])
            ->andFilterWhere(['like', 'Package2', $this->Package2])
            ->andFilterWhere(['like', 'Package3', $this->Package3])
            ->andFilterWhere(['like', 'Versions', $this->Versions])
            ->andFilterWhere(['like', 'DeviceToken', $this->DeviceToken])
            ->andFilterWhere(['like', 'gender', $this->gender])
            ->andFilterWhere(['like', 'payment', $this->payment])
            ->andFilterWhere(['like', 'payment2', $this->payment2])
            ->andFilterWhere(['like', 'remark', $this->remark])
            ->andFilterWhere(['like', 'auth_key', $this->auth_key]);

        return $dataProvider;
    }

    public function searchAssigUser($params,$assignUserData='',$expireDate='')
    {
        $query = Users::find();

        if ($assignUserData){
            $query = Users::find()
                ->andWhere(['UserID'=>$assignUserData]);
        }else{
            $query = Users::find()
                ->andWhere(['UserID'=>time()]);
        }

        // add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pagesize' => Yii::$app->session->get('usersPageSize',Yii::$app->params['listPerPage']),
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'UserExpiryDate' => $this->UserExpiryDate,
            'pre_mco_expiry' => $this->pre_mco_expiry,
            'pre_mco_expiry_vf' => $this->pre_mco_expiry_vf,
            'last_expiry' => $this->last_expiry,
            'UserLastUpdatedDate' => $this->UserLastUpdatedDate,
            'UserNextRegistrationDate' => $this->UserNextRegistrationDate,
            'UserRole' => $this->UserRole,
            'Admin' => $this->Admin,
            'LastLogin' => $this->LastLogin,
            'LastLogin2' => $this->LastLogin2,
            'WebSession' => $this->WebSession,
            'expiry2' => $this->expiry2,
            'last_expiry2' => $this->last_expiry2,
            'expiry3' => $this->expiry3,
            'Build' => $this->Build,
            'dob' => $this->dob,
            'age_group' => $this->age_group,
            'invest_exp' => $this->invest_exp,
            'special1' => $this->special1,
            'special2' => $this->special2,
            'special3' => $this->special3,
            'years1' => $this->years1,
            'years2' => $this->years2,
            'years3' => $this->years3,
            'sector1' => $this->sector1,
            'sector2' => $this->sector2,
            'sector3' => $this->sector3,
            'sector4' => $this->sector4,
            'hide_profile' => $this->hide_profile,
            'profile_update' => $this->profile_update,
            'profile_status' => $this->profile_status,
            'create_date' => $this->create_date,
            'self_create_date' => $this->self_create_date,
            'mtp_group' => $this->mtp_group,
        ]);

        $query->andFilterWhere(['like', 'UserID', $this->UserID])
            ->andFilterWhere(['like', 'UserID2', $this->UserID2])
            ->andFilterWhere(['like', 'UserPWD', $this->UserPWD])
            ->andFilterWhere(['like', 'UserKey', $this->UserKey])
            ->andFilterWhere(['like', 'UserName', $this->UserName])
            ->andFilterWhere(['like', 'SerialKey', $this->SerialKey])
            ->andFilterWhere(['like', 'SerialKeyM', $this->SerialKeyM])
            ->andFilterWhere(['like', 'SerialKeyV', $this->SerialKeyV])
            ->andFilterWhere(['like', 'referral_code', $this->referral_code])
            ->andFilterWhere(['like', 'agent_id', $this->agent_id])
            ->andFilterWhere(['like', 'Device', $this->Device])
            ->andFilterWhere(['like', 'SessionID', $this->SessionID])
            ->andFilterWhere(['like', 'Device2', $this->Device2])
            ->andFilterWhere(['like', 'SessionID2', $this->SessionID2])
            ->andFilterWhere(['like', 'Email', $this->Email])
            ->andFilterWhere(['like', 'Phone', $this->Phone])
            ->andFilterWhere(['like', 'IC', $this->IC])
            ->andFilterWhere(['like', 'Address', $this->Address])
            ->andFilterWhere(['like', 'Country', $this->Country])
            ->andFilterWhere(['like', 'Package', $this->Package])
            ->andFilterWhere(['like', 'Package2', $this->Package2])
            ->andFilterWhere(['like', 'Package3', $this->Package3])
            ->andFilterWhere(['like', 'Versions', $this->Versions])
            ->andFilterWhere(['like', 'DeviceToken', $this->DeviceToken])
            ->andFilterWhere(['like', 'gender', $this->gender])
            ->andFilterWhere(['like', 'payment', $this->payment])
            ->andFilterWhere(['like', 'payment2', $this->payment2])
            ->andFilterWhere(['like', 'remark', $this->remark])
            ->andFilterWhere(['like', 'auth_key', $this->auth_key]);

        return $dataProvider;
    }
}
