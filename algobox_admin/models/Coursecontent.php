<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "coursecontent".
 *
 * @property int $coursecontent_id
 * @property string $chapter_name
 * @property string|null $description
 * @property string|null $video_link
 * @property int|null $active
 * @property int $seq
 * @property int $course_id
 *
 * @property Course $course
 */
class Coursecontent extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'coursecontent';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['chapter_name', 'seq', 'course_id'], 'required'],
            [['active', 'seq', 'course_id'], 'integer'],
            [['chapter_name'], 'string', 'max' => 80],
            [['description'], 'string', 'max' => 500],
            [['video_link'], 'string', 'max' => 255],
            [['course_id', 'seq'], 'unique', 'targetAttribute' => ['course_id', 'seq']],
            [['course_id'], 'exist', 'skipOnError' => true, 'targetClass' => Course::className(), 'targetAttribute' => ['course_id' => 'course_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'coursecontent_id' => 'Coursecontent ID',
            'chapter_name' => 'Chapter Name',
            'description' => 'Description',
            'video_link' => 'Video Link',
            'active' => 'Active',
            'seq' => 'Seq',
            'course_id' => 'Course ID',
        ];
    }

    /**
     * Gets query for [[Course]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCourse()
    {
        return $this->hasOne(Course::className(), ['course_id' => 'course_id']);
    }

    public function getCourseName()
    {
        $c = $this->course;
        return !$c ? '' : $this->course->course_name;
    }
}
