<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "course".
 *
 * @property int $course_id
 * @property string $course_name
 * @property string|null $description
 * @property int|null $active
 * @property string $lastupdate
 *
 * @property Batch[] $batches
 * @property Coursecontent[] $coursecontents
 * @property Userbatch[] $userbatches
 */
class Course extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'course';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['course_name'], 'required'],
            [['active'], 'integer'],
            [['lastupdate'], 'safe'],
            [['course_name'], 'string', 'max' => 80],
            [['description'], 'string', 'max' => 500],
            [['course_name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'course_id' => 'Course ID',
            'course_name' => 'Course Name',
            'description' => 'Description',
            'active' => 'Active',
            'lastupdate' => 'Lastupdate',
        ];
    }

    /**
     * Gets query for [[Batches]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBatches()
    {
        return $this->hasMany(Batch::className(), ['course_id' => 'course_id']);
    }

    /**
     * Gets query for [[Coursecontents]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCoursecontents()
    {
        return $this->hasMany(Coursecontent::className(), ['course_id' => 'course_id']);
    }

    /**
     * Gets query for [[Userbatches]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUserbatches()
    {
        return $this->hasMany(Userbatch::className(), ['course_id' => 'course_id']);
    }

    public function getIsActive() {
        return $this->active == '1' ? 'Yes' : 'No';
    }
}
