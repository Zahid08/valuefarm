<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "users".
 *
 * @property string $UserID
 * @property string|null $UserID2
 * @property string $UserPWD
 * @property string|null $UserKey
 * @property string $UserName
 * @property string|null $UserExpiryDate
 * @property string|null $pre_mco_expiry
 * @property string|null $pre_mco_expiry_vf
 * @property string|null $last_expiry
 * @property string|null $UserLastUpdatedDate
 * @property string|null $UserNextRegistrationDate
 * @property string|null $SerialKey
 * @property string|null $SerialKeyM
 * @property string|null $SerialKeyV
 * @property int|null $UserRole
 * @property int|null $Admin
 * @property string|null $referral_code
 * @property string|null $agent_id
 * @property string|null $LastLogin
 * @property string|null $Device
 * @property string|null $SessionID
 * @property string|null $LastLogin2
 * @property string|null $Device2
 * @property string|null $SessionID2
 * @property string|null $WebSession
 * @property string|null $Email
 * @property string|null $Phone
 * @property string|null $IC
 * @property string|null $Address
 * @property string|null $Country
 * @property string|null $Package
 * @property string|null $Package2
 * @property string|null $expiry2
 * @property string|null $last_expiry2
 * @property string|null $Package3
 * @property string|null $expiry3
 * @property int|null $Build
 * @property string|null $Versions
 * @property string|null $DeviceToken
 * @property string|null $dob
 * @property string|null $gender
 * @property int|null $age_group
 * @property int|null $invest_exp
 * @property int|null $special1
 * @property int|null $special2
 * @property int|null $special3
 * @property int|null $years1
 * @property int|null $years2
 * @property int|null $years3
 * @property int|null $sector1
 * @property int|null $sector2
 * @property int|null $sector3
 * @property int|null $sector4
 * @property int|null $hide_profile
 * @property string|null $profile_update
 * @property int|null $profile_status
 * @property string|null $create_date
 * @property string|null $self_create_date
 * @property string|null $payment
 * @property string|null $payment2
 * @property string|null $remark
 * @property int|null $mtp_group
 * @property string|null $auth_key
 *
 * @property Activation[] $activations
 * @property Mtpdailystats[] $mtpdailystats
 * @property Paymentreceipt[] $paymentreceipts
 * @property Referralcode $referralcode
 * @property Registration[] $registrations
 * @property Registration[] $registrations0
 * @property Renewal[] $renewals
 * @property Usermodule[] $usermodules
 * @property Package $package
 * @property Package $package2
 * @property Package $package3
 * @property Agent $agent
 * @property Uservoucher[] $uservouchers
 */
class Users extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    public $showpassword = false;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['UserID', 'UserPWD', 'UserName'], 'required'],
            [['UserExpiryDate', 'pre_mco_expiry', 'pre_mco_expiry_vf', 'last_expiry', 'UserLastUpdatedDate', 'UserNextRegistrationDate', 'LastLogin', 'LastLogin2', 'WebSession', 'expiry2', 'last_expiry2', 'expiry3', 'dob', 'profile_update', 'create_date', 'self_create_date'], 'safe'],
            [['UserRole', 'Admin', 'Build', 'age_group', 'invest_exp', 'special1', 'special2', 'special3', 'years1', 'years2', 'years3', 'sector1', 'sector2', 'sector3', 'sector4', 'sector3', 'special1', 'special2', 'special3', 'hide_profile', 'profile_status', 'mtp_group'], 'integer'],
            [['Versions'], 'string'],
            [['UserID', 'UserID2', 'referral_code', 'agent_id', 'Device', 'Device2', 'Phone'], 'string', 'max' => 20],
            [['UserPWD', 'UserKey', 'UserName', 'SerialKey', 'SerialKeyM', 'SerialKeyV', 'SessionID', 'SessionID2', 'IC'], 'string', 'max' => 50],
            [['Email'], 'string', 'max' => 254],
            [['Address'], 'string', 'max' => 255],
            [['Country'], 'string', 'max' => 2],
            [['Package', 'Package2', 'Package3'], 'string', 'max' => 30],
            [['DeviceToken'], 'string', 'max' => 250],
            [['gender', 'payment', 'payment2'], 'string', 'max' => 1],
            [['remark'], 'string', 'max' => 1024],
            [['auth_key'], 'string', 'max' => 60],
            [['UserID2'], 'unique'],
            [['UserID'], 'unique'],
            [['Package'], 'exist', 'skipOnError' => true, 'targetClass' => Package::className(), 'targetAttribute' => ['Package' => 'package_id']],
            [['Package2'], 'exist', 'skipOnError' => true, 'targetClass' => Package::className(), 'targetAttribute' => ['Package2' => 'package_id']],
            [['Package3'], 'exist', 'skipOnError' => true, 'targetClass' => Package::className(), 'targetAttribute' => ['Package3' => 'package_id']],
//            [['agent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Agent::className(), 'targetAttribute' => ['agent_id' => 'agent_id']],
            [['UserID2', 'Email', 'Phone', 'Package', 'Package2', 'Package3', 'expiry2', 'expiry3', 'sector1', 'sector2', 'sector3', 'special1', 'special2', 'special3', 'hide_profile','remark'], 'default', 'value' => null],
            ['Phone',  'match', 'pattern'=>'/^[0-46-9]*[0-9]{7,12}$/', 'message' => 'Start with country code. Eg. 601234567'],
            [['UserExpiryDate', 'expiry2'],  'match', 'pattern'=>'/([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/', 'message' => 'Date format must be in yyyy-mm-dd'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'UserID' => 'User ID',
            'UserID2' => 'User Id2',
            'UserPWD' => 'Password',
            'UserKey' => 'User Key',
            'UserName' => 'User Name',
            'UserExpiryDate' => 'Expiry Date',
            'pre_mco_expiry' => 'Pre Mco Expiry',
            'pre_mco_expiry_vf' => 'Pre Mco Expiry Vf',
            'last_expiry' => 'Last Expiry',
            'UserLastUpdatedDate' => 'User Last Updated Date',
            'UserNextRegistrationDate' => 'User Next Registration Date',
            'SerialKey' => 'Serial Key',
            'SerialKeyM' => 'Serial Key M',
            'SerialKeyV' => 'Serial Key V',
            'UserRole' => 'User Role',
            'Admin' => 'Admin',
            'referral_code' => 'Referral Code',
            'agent_id' => 'Agent ID',
            'LastLogin' => 'Last Login',
            'Device' => 'Device',
            'SessionID' => 'Session ID',
            'LastLogin2' => 'Last Login2',
            'Device2' => 'Device2',
            'SessionID2' => 'Session Id2',
            'WebSession' => 'Web Session',
            'Email' => 'Email',
            'Phone' => 'Phone',
            'IC' => 'Ic',
            'Address' => 'Address',
            'Country' => 'Country',
            'Package' => 'Package',
            'Package2' => 'Package2',
            'expiry2' => 'Expiry Date 2',
            'last_expiry2' => 'Last Expiry2',
            'Package3' => 'Package3',
            'expiry3' => 'Expiry Date 3',
            'Build' => 'Build',
            'Versions' => 'Versions',
            'DeviceToken' => 'Device Token',
            'dob' => 'Dob',
            'gender' => 'Gender',
            'age_group' => 'Age Group',
            'invest_exp' => 'Invest Exp',
            'special1' => 'Special1',
            'special2' => 'Special2',
            'special3' => 'Special3',
            'years1' => 'Years1',
            'years2' => 'Years2',
            'years3' => 'Years3',
            'sector1' => 'Sector1',
            'sector2' => 'Sector2',
            'sector3' => 'Sector3',
            'sector4' => 'Sector4',
            'hide_profile' => 'Hide Profile',
            'profile_update' => 'Profile Update',
            'profile_status' => 'Profile Status',
            'create_date' => 'Create Date',
            'self_create_date' => 'Self Create Date',
            'payment' => 'Payment',
            'payment2' => 'Payment2',
            'remark' => 'Remark',
            'mtp_group' => 'Mtp Group',
            'auth_key' => 'Auth Key',
        ];
    }

    /**
     * Gets query for [[Activations]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getActivations()
    {
        return $this->hasMany(Activation::className(), ['user_id' => 'UserID']);
    }

    /**
     * Gets query for [[Mtpdailystats]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMtpdailystats()
    {
        return $this->hasMany(Mtpdailystats::className(), ['user_id' => 'UserID']);
    }

    /**
     * Gets query for [[Paymentreceipts]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentreceipts()
    {
        return $this->hasMany(Paymentreceipt::className(), ['user_id' => 'UserID']);
    }

    /**
     * Gets query for [[Referralcode]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getReferralcode()
    {
        return $this->hasOne(Referralcode::className(), ['user_id' => 'UserID']);
    }

    /**
     * Gets query for [[Registrations]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRegistrations()
    {
        return $this->hasMany(Registration::className(), ['user_id' => 'UserID']);
    }

    /**
     * Gets query for [[Registrations0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRegistrations0()
    {
        return $this->hasMany(Registration::className(), ['sender_id' => 'UserID']);
    }

    /**
     * Gets query for [[Renewals]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRenewals()
    {
        return $this->hasMany(Renewal::className(), ['user_id' => 'UserID']);
    }

    /**
     * Gets query for [[Usermodules]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsermodules()
    {
        return $this->hasMany(Usermodule::className(), ['user_id' => 'UserID']);
    }

    /**
     * Gets query for [[Package]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPackage()
    {
        return $this->hasOne(Package::className(), ['package_id' => 'Package']);
    }

    /**
     * Gets query for [[Package2]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPackage2()
    {
        return $this->hasOne(Package::className(), ['package_id' => 'Package2']);
    }

    /**
     * Gets query for [[Package3]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPackage3()
    {
        return $this->hasOne(Package::className(), ['package_id' => 'Package3']);
    }

    /**
     * Gets query for [[Agent]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAgent()
    {
        return $this->hasOne(Agent::className(), ['agent_id' => 'agent_id']);
    }

    /**
     * Gets query for [[Uservouchers]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUservouchers()
    {
        return $this->hasMany(Uservoucher::className(), ['user_id' => 'UserID']);
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->auth_key = \Yii::$app->security->generateRandomString();
            }
            return true;
        }
        return false;
    }
    
    public function findByUsername($username)
    {
        return self::findOne(['UserID' => $username]);
    }

    public function validatePassword($password)
    {
        return $this->UserPWD === $password;
    }

    public function getId()
    {
        return $this->UserID;
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function validateAuthKey($authKey)
    {
        return $this->auth_key === $authKey;
    }

    public static function findIdentity($id)
    {
        return static::findOne(['UserID' => $id]);
    }
    
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return null;
    }

    public function getUserRoleText()
    {
        if ($this->UserRole == Yii::$app->params['superuser']) return 'Superuser';
        else if ($this->UserRole == Yii::$app->params['administrator']) return 'Admin';
        else if ($this->UserRole == Yii::$app->params['supervisor']) return 'Supervisor';
        else if ($this->UserRole == Yii::$app->params['operatortester']) return 'Operator Tester';
        else if ($this->UserRole == Yii::$app->params['research']) return 'Research';
        else if ($this->UserRole == Yii::$app->params['operator']) return 'Operator';
        else return Yii::$app->formatter->nullDisplay;
    }
}
