<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "batch".
 */
class MappingPackage extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mapping_users_package';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'safe'],
            [['package_name', 'expire'], 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'package_name' => 'Batch Name',
            'expire' => 'Batch No'
        ];
    }
}
