<?php

return [
    'adminEmail' => 'admin@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',

    'listPerPage' => 20,
    'pageSizeOptions' => [10 => 10, 20 => 20, 50 => 50, 100 => 100, 200 => 200],

    //role
    'operator' => 1,
    'research' => 2,
    'operatortester' => 3,
    'supervisor' => 4,
    'administrator' => 8,
    'superuser' => 9,
];
