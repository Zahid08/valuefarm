<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Coursecontent */

$this->title = 'Create Coursecontent';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Coursecontent'), 'url' => ['course/index']];
if ($id > 0)
    $this->params['breadcrumbs'][] = ['label' => $model->courseName, 'url' => ['course/view', 'id' => $model->course_id]];
    
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="categoryitem-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model, 'id' => $id,
    ]) ?>

</div>
