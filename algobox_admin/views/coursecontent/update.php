<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Coursecontent */

$courseModel=\app\models\Course::findOne($model->course_id);
$courseName=$courseId='';
if ($courseModel){
    $courseName=$courseModel->course_name;
}

$this->title = 'Update Coursecontent: ' . $model->chapter_name;
$this->params['breadcrumbs'][] = ['label' => 'Course', 'url' => ['course/index']];
$this->params['breadcrumbs'][] = ['label' => $courseName, 'url' => ['course/view', 'id' => $model->course_id]];
$this->params['breadcrumbs'][] = 'Update';

?>
<div class="coursecontent-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model, 'id' => $id,
    ]) ?>

</div>
