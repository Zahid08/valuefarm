<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Coursecontent */

$courseModel=\app\models\Course::findOne($model->course_id);
$courseName=$courseId='';
if ($courseModel){
    $courseName=$courseModel->course_name;
}

$this->title = $model->chapter_name;
$this->params['breadcrumbs'][] = ['label' => 'Course', 'url' => ['course/index']];
$this->params['breadcrumbs'][] = ['label' => $courseName, 'url' => ['course/view', 'id' => $model->course_id]];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="coursecontent-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('<< Back', ['course/index'], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Update', ['update', 'id' => $model->coursecontent_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->coursecontent_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'coursecontent_id',
            'chapter_name',
            'description',
            'video_link',
            ['attribute' => 'active',
            'value' => function ($model) {
                return $model->active ? "Yes" : "No";
            },
            'contentOptions' => ['style' => $model->active ? 'color:green' :'color:red']],
            'seq',
            'course_id',
        ],
    ]) ?>

</div>
