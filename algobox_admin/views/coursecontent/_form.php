<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Course;

/* @var $this yii\web\View */
/* @var $model app\models\Coursecontent */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="coursecontent-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php 
        if ($id == 0)
            echo $form->field($model, 'course_id')->dropDownList(ArrayHelper::map(Course::find()->all(),'course_id', 'course_name'), ['prompt' => 'Select a course']);
        else {
            echo $form->field($model, 'courseName')->textInput(['readOnly' => true])->label('Course');
            echo $form->field($model, 'course_id')->hiddenInput()->label(false);
        }
        ?>

    <?= $form->field($model, 'chapter_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'video_link')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'active')->dropDownList(['1'=>'Yes', '0'=>'No']); ?>  

    <?= $form->field($model, 'seq')->textInput() ?>
    

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
