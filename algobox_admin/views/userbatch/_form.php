<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Userbatch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="userbatch-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'course_id')->dropDownList(ArrayHelper::map(Course::find()->all(),'course_id', 'course_name'), ['prompt' => 'Select Course ID']); ?>

    <?= $form->field($model, 'course_id')->dropDownList(ArrayHelper::map(Batch::find()->all(),'Batch_id', 'batch_name'), ['prompt' => 'Select Batch ID']); ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
