<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Userbatch */

$this->title = 'Create Userbatch';
$this->params['breadcrumbs'][] = ['label' => 'Userbatches', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="userbatch-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
