<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Userbatches';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="userbatch-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Userbatch', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'userbatch_id',
            'user_id',
            'course_id',
            'batch_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
