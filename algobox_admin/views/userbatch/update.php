<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Userbatch */

$this->title = 'Update Userbatch: ' . $model->userbatch_id;
$this->params['breadcrumbs'][] = ['label' => 'Userbatches', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->userbatch_id, 'url' => ['view', 'id' => $model->userbatch_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="userbatch-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
