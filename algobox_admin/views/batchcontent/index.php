<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Batchcontents';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="batchcontent-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Batchcontent', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'batchcontent_id',
            'description',
            'coursecontent_id',
            ['attribute' => 'start_time', 'format' => 'raw', 'value' => function ($model, $key, $index, $column) {return $model->dateText;}],
            ['attribute' => 'end_time', 'format' => 'raw', 'value' => function ($model, $key, $index, $column) {return $model->dateText;}],
            //'video_link',
            //'active',
            //'seq',
            //'batch_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
