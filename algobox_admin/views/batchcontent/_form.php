<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Batch;
use app\models\Coursecontent;
use kartik\date\DatePicker;
use kartik\datetime\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Batchcontent */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="batchcontent-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php 
        if ($id == 0)
            echo $form->field($model, 'batch_id')->dropDownList(ArrayHelper::map(Batch::find()->all(),'batch_id', 'batch_name'), ['prompt' => 'Select a Bacth']);
        
        
        else {
            echo $form->field($model, 'batchName')->textInput(['readOnly' => true])->label('Batch');
            echo $form->field($model, 'batch_id')->hiddenInput()->label(false);
        }
        ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <?php //= $form->field($model, 'coursecontent_id')->textInput() ?>

    <?= $form->field($model, 'coursecontent_id')->dropDownList(ArrayHelper::map(Coursecontent::find()->all(),'coursecontent_id', 'chapter_name'), ['prompt' => 'Select Course Content']); ?>

    <?php echo $form->field($model, 'start_time')->widget(
        DateTimePicker::class,
        [
            'options' => ['placeholder' => 'Select operating time ...'],
            'convertFormat' => true,
            'pluginOptions' => [
                'todayHighlight' => true,
                'todayBtn' => true,
                'format' => 'yyyy-M-dd H:i:s',
                'autoclose' => true,
                'startDate'=> date('d-m-Y H:i:s', strtotime(time())),
            ]
        ]
    );
    ?>

    <?php echo $form->field($model, 'end_time')->widget(
        DateTimePicker::class,
        [
            'options' => ['placeholder' => 'Select operating time ...'],
            'convertFormat' => true,
            'pluginOptions' => [
                'todayHighlight' => true,
                'todayBtn' => true,
                'format' => 'yyyy-M-dd H:i:s',
                'autoclose' => true,
                'startDate'=> date('d-m-Y H:i:s', strtotime(time())),
            ]
        ]
    );
    ?>

    <?= $form->field($model, 'video_link')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'active')->dropDownList(['1'=>'Yes', '0'=>'No']); ?>  

    <?= $form->field($model, 'seq')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
