<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Batchcontent */

$batchModel=\app\models\Batch::findOne($model->batch_id);
$courseName=$courseId=$batchName='';
if ($batchModel){
    $batchName=$batchModel->batch_name;
    $courseModel=\app\models\Course::findOne($batchModel->course_id);
    if ($courseModel){
        $courseName=$courseModel->course_name;
        $courseId=$courseModel->course_id;
    }
}

$this->title = 'Update Batchcontent: ' . $model->batchcontent_id;
$this->params['breadcrumbs'][] = ['label' =>'Courses', 'url' => ['course/index']];
$this->params['breadcrumbs'][] = ['label' => $courseName, 'url' => ['course/view', 'id' => $courseId]];
$this->params['breadcrumbs'][] = ['label' => 'Batches', 'url' => ['batch/index','id' => $courseId]];
$this->params['breadcrumbs'][] = ['label' => $batchName, 'url' => ['batch/view', 'id' => $model->batch_id,'courseId'=>$courseId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="batchcontent-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model, 'id' => $id,
    ]) ?>

</div>
