<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Batchcontent */

$this->title = 'Create Batchcontent';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Batchcontent'), 'url' => ['batch/index']];
if ($id > 0)
    $this->params['breadcrumbs'][] = ['label' => $model->batchName, 'url' => ['batch/view', 'id' => $model->batch_id]];
    
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="categoryitem-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model, 'id' => $id,
    ]) ?>

</div>
