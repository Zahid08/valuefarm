<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Batchcontent */

$batchModel=\app\models\Batch::findOne($model->batch_id);
$courseName=$courseId=$batchName='';
if ($batchModel){
    $batchName=$batchModel->batch_name;
    $courseModel=\app\models\Course::findOne($batchModel->course_id);
    if ($courseModel){
        $courseName=$courseModel->course_name;
        $courseId=$courseModel->course_id;
    }
}

$this->title = $model->description;
$this->params['breadcrumbs'][] = ['label' =>'Courses', 'url' => ['course/index']];
$this->params['breadcrumbs'][] = ['label' => $courseName, 'url' => ['course/view', 'id' => $courseId]];
$this->params['breadcrumbs'][] = ['label' => 'Batches', 'url' => ['batch/index','id' => $courseId]];
$this->params['breadcrumbs'][] = ['label' => $batchName, 'url' => ['batch/view', 'id' => $model->batch_id,'courseId'=>$courseId]];

$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="batchcontent-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('<< Back', ['batch/view', 'id' => $model->batch_id,'courseId'=>$courseId], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Update', ['update', 'id' => $model->batchcontent_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->batchcontent_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'batchcontent_id',
            'description',
            //'coursecontent_id',
            //'start_time',
            ['attribute' =>'start_time', 'value' => function($model) {return $model->startdateText;}],
            ['attribute' =>'end_time', 'value' => function($model) {return $model->enddateText;}],
            'video_link',
            ['attribute' => 'active',
            'value' => function ($model) {
                return $model->active ? "Yes" : "No";
            },
            'contentOptions' => ['style' => $model->active ? 'color:green' :'color:red']],
            //'seq',
            //'batch_id',
        ],
    ]) ?>

</div>
