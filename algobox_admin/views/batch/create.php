<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Batch */

$model->course_id=isset($_REQUEST['id'])?$_REQUEST['id']:'';
$courseModel=\app\models\Course::findOne($model->course_id);
$courseName=$courseId='';
if ($courseModel){
    $courseName=$courseModel->course_name;
}

$this->title = 'Create Batch';
$this->params['breadcrumbs'][] = ['label' =>'Courses', 'url' => ['course/index']];
$this->params['breadcrumbs'][] = ['label' => $courseName, 'url' => ['course/view', 'id' => $model->course_id]];
$this->params['breadcrumbs'][] = ['label' => 'Batches', 'url' => ['index','id' => $model->course_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="batch-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
