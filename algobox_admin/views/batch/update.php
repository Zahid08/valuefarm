<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Batch */
$courseModel=\app\models\Course::findOne($model->course_id);
$courseName=$courseId='';
if ($courseModel){
    $courseName=$courseModel->course_name;
}

$this->title = 'Update Batch: ' . $model->batch_id;
$this->params['breadcrumbs'][] = ['label' =>'Courses', 'url' => ['course/index']];
$this->params['breadcrumbs'][] = ['label' => $courseName, 'url' => ['course/view', 'id' => $model->course_id]];
$this->params['breadcrumbs'][] = ['label' => 'Batches', 'url' => ['index','id' => $model->course_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="batch-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
