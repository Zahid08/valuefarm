<?php

namespace app\models;

use Yii;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$batchId=isset($_REQUEST['id'])?$_REQUEST['id']:'';

$batchModel=\app\models\Batch::findOne($batchId);
$courseName=$courseId=$batchName='';
if ($batchModel){
    $batchName=$batchModel->batch_name;
    $courseModel=\app\models\Course::findOne($batchModel->course_id);
    if ($courseModel){
        $courseName=$courseModel->course_name;
        $courseId=$courseModel->course_id;
    }
}

$this->title = 'Users';
$this->params['breadcrumbs'][] = ['label' =>'Courses', 'url' => ['course/index']];
$this->params['breadcrumbs'][] = ['label' => $courseName, 'url' => ['course/view', 'id' => $courseId]];
$this->params['breadcrumbs'][] = ['label' => 'Batches', 'url' => ['batch/index','id'=>$courseId]];
$this->params['breadcrumbs'][] = ['label' => $batchName, 'url' => ['batch/view','id'=>$batchId]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-index">

    <h1><?= Html::encode($this->title) .' : '.$batchName ?></h1>

    <p>
        <?= Html::a('Assign user into batch', ['users/index', 'batchId' => $batchId], ['class' => 'btn btn-success']) ?>
    </p>

    <?php
    //$role = Yii::$app->session->get('user.role');
    $role = Users::findOne(Yii::$app->user->id)->UserRole;
    $pageSize=Yii::$app->session->get('usersPageSize',Yii::$app->params['listPerPage']);
    ?>

    <?php
    if (/*$role >= Yii::$app->params['superuser'] || */$role >= Yii::$app->params['administrator']) {
        $template = '{view}{update}{delete}';
        $rTemplate = [Yii::$app->params['superuser'] => 'Superuser',
            Yii::$app->params['administrator'] => 'Admin',
            Yii::$app->params['supervisor'] => 'Supervisor',
            Yii::$app->params['operatortester'] => 'Operator Tester',
            Yii::$app->params['research'] => 'Research',
            Yii::$app->params['operator'] => 'Operator'];
    }/* else if ($role == Yii::$app->params['manager']) {
        $template = '{view}{update}';
        $rTemplate = [Yii::$app->params['manager'] => 'Manager',
                    Yii::$app->params['leader'] => 'BA',
                    Yii::$app->params['agent'] => 'Agent'];
    }*/ else if ($role >= Yii::$app->params['operator']){
        $template = '{view}';
        $rTemplate = [];
    } else {
        $template = '';
        $rTemplate = [];
    }
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'filterSelector' => '#pageSize',
        'emptyCell'=>'-',
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            'UserID',
            //'UserID2',
            //'UserPWD',
            //'UserKey',
            ['attribute' => 'UserName',
                'format' => 'raw',
                'value' => function($model){return !$model->UserName ? Yii::$app->formatter->nullDisplay : $model->UserName.'<BR><small>'.
                    (!$model->Email ? Yii::$app->formatter->nullDisplay : $model->Email).'</small>';},
                'headerOptions' => ['style'=>'min-width: 180px;']
            ],
            'LastLogin',
//            ['attribute' => 'Package',
//                'label' => 'Package 1',
//                'format' => 'raw',
//                'value' => function($model){return !$model->Package ? Yii::$app->formatter->nullDisplay : $model->Package.'<BR><small>Exp: '.
//                    (!$model->UserExpiryDate ? Yii::$app->formatter->nullDisplay : $model->UserExpiryDate).'</small>';}
//            ],
//            ['attribute' => 'Package2',
//                'label' => 'Package 2',
//                'format' => 'raw',
//                'value' => function($model){return !$model->Package2 ? Yii::$app->formatter->nullDisplay : $model->Package2.'<BR><small>Exp: '.
//                    (!$model->expiry2 ? Yii::$app->formatter->nullDisplay : $model->expiry2).'</small>';}
//            ],
            ['attribute' => 'Build', 'headerOptions' => ['style'=>'width: 70px;']],
            ['attribute' => 'referral_code', 'label' => 'Ref. Code'],
            //'UserExpiryDate',
            //'pre_mco_expiry',
            //'pre_mco_expiry_vf',
            //'last_expiry',
            //'UserLastUpdatedDate',
            //'UserNextRegistrationDate',
            //'SerialKey',
            //'SerialKeyM',
            //'SerialKeyV',
            //'UserRole',
            //'Admin',
            //'referral_code',
            'agent_id',
            //'Device',
            //'SessionID',
            //'LastLogin2',
            //'Device2',
            //'SessionID2',
            //'WebSession',
            //'Email:email',
            //'Phone',
            //'IC',
            //'Address',
            //'Country',
            //'Package',
            //'Package2',
            //'expiry2',
            //'last_expiry2',
            //'Package3',
            //'expiry3',
            //'Build',
            //'Versions:ntext',
            //'DeviceToken',
            //'dob',
            //'gender',
            //'age_group',
            //'invest_exp',
            //'special1',
            //'special2',
            //'special3',
            //'years1',
            //'years2',
            //'years3',
            //'sector1',
            //'sector2',
            //'sector3',
            //'sector4',
            //'hide_profile',
            //'profile_update',
            //'profile_status',
            'create_date',
            //'self_create_date',
            //'payment',
            //'payment2',
            //'remark',
            //'mtp_group',
            //'auth_key',
        ],
    ]); ?>


</div>

<style>
    /*.thead-blue table thead {
        background-color: cornflowerblue;
    }
    .grid-view table thead {
        background-color: #cccccc;
    }

    .grid-view table thead a {
        color: #2e2eb8;
    }*/

    .grid-view table tr:hover {
        background-color: #E9E9E9;
    }

    .table-striped {
        background-color: #F5F5F5;
    }

    .grid-view table tr td {
        padding: 5px;
    }
</style>


