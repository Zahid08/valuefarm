<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Batch */

$courseModel=\app\models\Course::findOne($model->course_id);
$courseName=$courseId='';
if ($courseModel){
    $courseName=$courseModel->course_name;
}

$this->title = $model->batch_name;
$this->params['breadcrumbs'][] = ['label' =>'Courses', 'url' => ['course/index']];
$this->params['breadcrumbs'][] = ['label' => $courseName, 'url' => ['course/view', 'id' => $model->course_id]];
$this->params['breadcrumbs'][] = ['label' => 'Batches', 'url' => ['batch/index','id'=>$model->course_id]];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="batch-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('<< Back', ['batch/index','id'=>$model->course_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Update', ['update', 'id' => $model->batch_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->batch_id,'courseId'=>$model->course_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('User batch', ['batch/userbatch', 'id' => $model->batch_id], ['class' => 'btn btn-success']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'batch_id',
            'batch_name',
            //'batch_no',
            'date',
            ['attribute' => 'active',
            'value' => function ($model) {
                return $model->active ? "Yes" : "No";
            },
            'contentOptions' => ['style' => $model->active ? 'color:green' :'color:red']],
            'description',
            ['attribute' => 'package',
                'label' => 'Package',
                'format' => 'raw',
                'value' => function($model){return !$model->package ? Yii::$app->formatter->nullDisplay : $model->package.'<BR><small>Exp: '.
                    (!$model->expiry ? Yii::$app->formatter->nullDisplay : $model->expiry).'</small>';}
            ],
            //'course_id',
            //'lastupdate',
        ],
    ]) ?>

    <h2>Timetable</h2>

    <p>
        <?= Html::a('Create Batch content', ['batchcontent/create', 'id' => $model->batch_id], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'batchcontent_id',
            ['attribute' => 'coursecontent_id',
                'label' => 'Chapter Name',
                'value' => function ($model) {
                    $getChapterNameFromCourseContent=\app\models\Coursecontent::findOne($model->coursecontent_id);
                    if ($getChapterNameFromCourseContent){
                        return $getChapterNameFromCourseContent->chapter_name;
                    }else{
                        return "";
                    }
                },
            ],
            'description',
            'start_time',
            'end_time',
            'video_link',
            ['attribute' => 'active',
                'value' => function ($model) {
                    return $model->active ? "Yes" : "No";
                },
                'contentOptions' => function ($model, $key, $index, $column) { 
                    return $model->active ? ['style' => 'color:green'] : ['style' => 'color:red']; 
            }],
            //'seq',
            //'batch_id',

            ['class' => 'yii\grid\ActionColumn',
             
             'contentOptions' => ['class' => 'text-center'],
             'buttons' => [
                'view' => function ($url,$model) {
                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>',['batchcontent/view', 'id' => $model->batchcontent_id]);
                },
                'update' => function ($url,$model) {
                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>',['batchcontent/update', 'id' => $model->batchcontent_id]);
                },
                'delete' => function ($url,$model) {
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>',['batchcontent/delete', 'id' => $model->batchcontent_id],
                        ['data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ]);
                },
            ]],
        ],
    ]); ?>

</div>
