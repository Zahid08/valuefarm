<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Course;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Batch */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
    select#batch-course_id {
        pointer-events: none;
    }
</style>

<div class="batch-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'batch_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'batch_no')->textInput() ?>

    <?= $form->field($model, 'date')->widget(
        DatePicker::className(), [
        'name' => 'date',
        'value' => '12-31-2010',
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd'
        ]
    ]);?>

    <?= $form->field($model, 'active')->dropDownList(['1'=>'Yes', '0'=>'No']); ?>   

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <?php
        if ( $model->isNewRecord){
            $model->course_id=isset($_REQUEST['id'])?$_REQUEST['id']:'';
        }
    ?>
    <?= $form->field($model, 'course_id')->dropDownList(ArrayHelper::map(Course::find()->all(),'course_id', 'course_name'), ['prompt' => 'Select Course ID','readOnly' => true]); ?>

    <?= $form->field($model, 'lastupdate')->textInput(['readOnly' => true]) ?>

    <?= $form->field($model, 'package')->dropDownList(ArrayHelper::map(\app\models\Package::find()->all(),'package_id', 'package_id'), ['prompt' => 'Select Package']); ?>

    <?= $form->field($model, 'expiry')->widget(
        DatePicker::className(), [
        'name' => 'date',
        'value' => '12-31-2010',
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd'
        ]
    ]);?>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
