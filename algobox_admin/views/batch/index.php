<?php

use yii\helpers\Html;
use yii\grid\GridView;


/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$courseName=$courseId='';
if ($courseModel){
    $courseName=$courseModel->course_name;
    $courseId=$courseModel->course_id;
}
$this->title = 'Batches';
$this->params['breadcrumbs'][] = ['label' =>'Courses', 'url' => ['course/index']];
$this->params['breadcrumbs'][] =['label' =>$courseName, 'url' => ['course/view/', 'id' => $courseId]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="batch-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Batch', ['create','id'=>$id], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'batch_id',
            ['attribute' => 'batch_name', 
             'headerOptions' => ['width' => '200px'],
             'format' => 'raw', 
             'value' => function ($model) {
                return Html::a($model->batch_name,['batch/view', 'id'=>$model->batch_id,'courseId' => $model->course_id]);
            }],
            //'batch_no',
            ['attribute' => 'date', 
             'format' => 'raw',
             'value' => function($model){return $model->date;}
            ],
            
            [
                'attribute' => 'active',
                'value' => function ($model) {
                    return $model->active ? "Yes" : "No";
                },
                'contentOptions' => function ($model, $key, $index, $column) { 
                    return $model->active ? ['style' => 'color:green'] : ['style' => 'color:red']; 
                }
            ],
            'description',
            'course_id',
            //'lastupdate',
            ['class' => 'yii\grid\ActionColumn',

                'contentOptions' => ['class' => 'text-center'],
                'buttons' => [
                    'view' => function ($url,$model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>',['batch/view', 'id' => $model->batch_id]);
                    },
                    'update' => function ($url,$model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>',['batch/update', 'id' => $model->batch_id]);
                    },
                    'delete' => function ($url,$model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>',['batch/delete', 'id' => $model->batch_id,'courseId' => $_REQUEST['id']],
                            ['data' => [
                                'confirm' => 'Are you sure you want to delete this item?',
                                'method' => 'post',
                            ],
                            ]);
                    },
                ]],
        ],
    ]); ?>


</div>
