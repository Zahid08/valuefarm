<?php

namespace app\models;

use Yii;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$batchId=isset($_REQUEST['batchId'])?$_REQUEST['batchId']:'';
if ($batchId){
    $batchModel=\app\models\Batch::findOne($batchId);
    $courseName=$courseId=$batchName='';
    if ($batchModel){
        $batchName=$batchModel->batch_name;
        $courseModel=\app\models\Course::findOne($batchModel->course_id);
        if ($courseModel){
            $courseName=$courseModel->course_name;
            $courseId=$courseModel->course_id;
        }
    }

    $this->title = 'Users';
    $this->params['breadcrumbs'][] = ['label' =>'Courses', 'url' => ['course/index']];
    $this->params['breadcrumbs'][] = ['label' => $courseName, 'url' => ['course/view', 'id' => $courseId]];
    $this->params['breadcrumbs'][] = ['label' => 'Batches', 'url' => ['batch/index','id'=>$courseId]];
    $this->params['breadcrumbs'][] = ['label' => $batchName, 'url' => ['batch/view','id'=>$batchId]];
    $this->params['breadcrumbs'][] = $this->title;
}else {
    $this->title = 'Users';
    $this->params['breadcrumbs'][] = $this->title;
}

?>
<style>
    p#successMessage {
        font-size: 20px;
    }
</style>
<div class="users-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php
        //$role = Yii::$app->session->get('user.role');
        $role = Users::findOne(Yii::$app->user->id)->UserRole;
        $pageSize=Yii::$app->session->get('usersPageSize',Yii::$app->params['listPerPage']);
    ?>

    <p>
        <?php 
            if ($role >= Yii::$app->params['administrator'] && empty($batchId)) {
                echo Html::a('Create Users', ['create'], ['class' => 'btn btn-success']);
            }
            if (!empty($batchId)){ ?>
                <?= Html::a('<< Back', ['batch/userbatch','id'=>$batchId], ['class' => 'btn btn-primary']) ?>
                <button type="button" class="btn btn-success" id="checkAllSaveChange" data-batchId="<?=$batchId?>">Save Change</button>
                <p id="successMessage"></p>
                <input type="hidden" id="batchIdValue" value="<?=$batchId?>">
            <?php }
        ?>
    </p>

    <?php
    if (/*$role >= Yii::$app->params['superuser'] || */$role >= Yii::$app->params['administrator']) {
        $template = '{view}{update}{delete}';
        $rTemplate = [Yii::$app->params['superuser'] => 'Superuser', 
                    Yii::$app->params['administrator'] => 'Admin', 
                    Yii::$app->params['supervisor'] => 'Supervisor',
                    Yii::$app->params['operatortester'] => 'Operator Tester',
                    Yii::$app->params['research'] => 'Research',
                    Yii::$app->params['operator'] => 'Operator'];
    }/* else if ($role == Yii::$app->params['manager']) {
        $template = '{view}{update}';
        $rTemplate = [Yii::$app->params['manager'] => 'Manager',
                    Yii::$app->params['leader'] => 'BA',
                    Yii::$app->params['agent'] => 'Agent'];
    }*/ else if ($role >= Yii::$app->params['operator']){
        $template = '{view}';
        $rTemplate = [];
    } else {
        $template = '';
        $rTemplate = [];
    }
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'filterSelector' => '#pageSize',
        'emptyCell'=>'-',
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            'UserID',
            //'UserID2',
            //'UserPWD',
            //'UserKey',
            ['attribute' => 'UserName', 
            'format' => 'raw',
            'value' => function($model){return !$model->UserName ? Yii::$app->formatter->nullDisplay : $model->UserName.'<BR><small>'.
                                       (!$model->Email ? Yii::$app->formatter->nullDisplay : $model->Email).'</small>';},
             'headerOptions' => ['style'=>'min-width: 180px;']
            ],
            'LastLogin',
//            ['attribute' => 'UserID',
//             'label' => 'Package 1',
//             'format' => 'raw',
//             'value' => function($model) {
//                 $mappingPackageModel = MappingPackage::find()->where(['user_id' => $model->UserID])->orderBy(['id' => SORT_DESC])->one();
//                 if ($mappingPackageModel) {
//                     return !$mappingPackageModel->package_name ? Yii::$app->formatter->nullDisplay : $mappingPackageModel->package_name . '<BR><small>Exp: ' .
//                         (!$mappingPackageModel->expire ? Yii::$app->formatter->nullDisplay : $mappingPackageModel->expire) . '</small>';
//                 } else {
//                     return Yii::$app->formatter->nullDisplay;
//                 }
//             }
//            ],
            ['attribute' => 'Build', 'headerOptions' => ['style'=>'width: 70px;']],
            ['attribute' => 'referral_code', 'label' => 'Ref. Code'],
            //'UserExpiryDate',
            //'pre_mco_expiry',
            //'pre_mco_expiry_vf',
            //'last_expiry',
            //'UserLastUpdatedDate',
            //'UserNextRegistrationDate',
            //'SerialKey',
            //'SerialKeyM',
            //'SerialKeyV',
            //'UserRole',
            //'Admin',
            //'referral_code',
            'agent_id',
            //'Device',
            //'SessionID',
            //'LastLogin2',
            //'Device2',
            //'SessionID2',
            //'WebSession',
            //'Email:email',
            //'Phone',
            //'IC',
            //'Address',
            //'Country',
            //'Package',
            //'Package2',
            //'expiry2',
            //'last_expiry2',
            //'Package3',
            //'expiry3',
            //'Build',
            //'Versions:ntext',
            //'DeviceToken',
            //'dob',
            //'gender',
            //'age_group',
            //'invest_exp',
            //'special1',
            //'special2',
            //'special3',
            //'years1',
            //'years2',
            //'years3',
            //'sector1',
            //'sector2',
            //'sector3',
            //'sector4',
            //'hide_profile',
            //'profile_update',
            //'profile_status',
            'create_date',
            //'self_create_date',
            //'payment',
            //'payment2',
            //'remark',
            //'mtp_group',
            //'auth_key',
            [
                'class' => 'yii\grid\CheckboxColumn',
                'header' => Html::checkBox('fdf', false, [
                    'class' => 'select-on-check-all'
                ]),
                'checkboxOptions' => function($model) {
                    $batchIdRequest=isset($_REQUEST['batchId'])?$_REQUEST['batchId']:'';

                    $checedStatus=false;
                    if ($batchIdRequest){
                        $gettingBatchModel=Batch::findOne($batchIdRequest);
                        $userAssign=(array)json_decode($gettingBatchModel->assign_users);
                        if (in_array($model->UserID, $userAssign)) {
                            $checedStatus=true;
                        }

                    }
                    $userAssingChecked = ["value" => $model->UserID, "class" => 'user-status', "label" => '', "checked" => $checedStatus];
                    return $userAssingChecked;
                },
            ],
            ['class' => 'yii\grid\ActionColumn',
             //'template' => $template,
             'header'=>Html::dropDownList('pageSize', $pageSize, Yii::$app->params['pageSizeOptions'], ['id' => 'pageSize', 'style' => 'margin-top:6px;']),
             'contentOptions' => ['class' => 'text-center'],
            ], 
        ],
    ]); ?>


</div>

<style>
    /*.thead-blue table thead {
        background-color: cornflowerblue;
    }
    .grid-view table thead {
        background-color: #cccccc;
    }

    .grid-view table thead a {
        color: #2e2eb8;
    }*/

    .grid-view table tr:hover {
        background-color: #E9E9E9;
    }

    .table-striped {
        background-color: #F5F5F5;
    }

    .grid-view table tr td {
        padding: 5px;
    }
</style>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript">
    $( document ).ready(function() {
        $('.select-on-check-all').change(function () {
            $('.user-status').prop('checked',this.checked);
        });

        $('.user-status').change(function () {
            if ($('.user-status:checked').length == $('.user-status').length){
                $('#select-on-check-all').prop('checked',true);
            }
            else {
                $('#select-on-check-all').prop('checked',false);
            }
        });

        var checkBatchIdexistOrNot=$('#batchIdValue').val();
        if (checkBatchIdexistOrNot) {
            $('td:nth-child(8)').show();
            $('th:nth-child(8)').show();
            $('td:nth-child(9)').hide();
            $('th:nth-child(9)').hide();
        }else {
            $('td:nth-child(8)').hide();
            $('th:nth-child(8)').hide();
            $('td:nth-child(9)').show();
            $('th:nth-child(9)').show();
        }

        $('#checkAllSaveChange').off('click', '.checkAllSaveChange').on('click',function(e){
            var parsingDataList = [];
            var batchId=$('#batchIdValue').val();
            //Checked Items Push Into List
            $(".user-status:checked").each(function(){
                parsingDataList.push($(this).val());
            });

            if (parsingDataList.length>0) {
                $.ajax({
                    type: 'post',
                    url: '<?php echo Yii::$app->request->baseUrl . '/index.php?r=batch%2Fupdate-assign-users'?>',
                    data: {selection: parsingDataList, batchIdValue: batchId},
                    success: function (response) {
                        if (response == 1) {

                            $('#successMessage').html('Your selected item saved successfully');
                            $('#successMessage').attr("style", "color:green");

                            setTimeout(function () {
                                $('#successMessage').html("");
                            }, 3000);
                        }else {
                            $('#successMessage').html('Something missing try again');
                            $('#successMessage').attr("style", "color:red");
                            setTimeout(function () {
                                $('#successMessage').html("");
                            }, 3000);
                        }
                    }, beforeSend: function (xhr) {
                    },
                });
            }else {
                $('#successMessage').html('Assign at least one');
                $('#successMessage').attr("style", "color:red");
                setTimeout(function () {
                    $('#successMessage').html("");
                }, 3000);
            }
        });
    });
</script>