<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UsersSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="users-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'UserID') ?>

    <?= $form->field($model, 'UserID2') ?>

    <?= $form->field($model, 'UserPWD') ?>

    <?= $form->field($model, 'UserKey') ?>

    <?= $form->field($model, 'UserName') ?>

    <?php // echo $form->field($model, 'UserExpiryDate') ?>

    <?php // echo $form->field($model, 'pre_mco_expiry') ?>

    <?php // echo $form->field($model, 'pre_mco_expiry_vf') ?>

    <?php // echo $form->field($model, 'last_expiry') ?>

    <?php // echo $form->field($model, 'UserLastUpdatedDate') ?>

    <?php // echo $form->field($model, 'UserNextRegistrationDate') ?>

    <?php // echo $form->field($model, 'SerialKey') ?>

    <?php // echo $form->field($model, 'SerialKeyM') ?>

    <?php // echo $form->field($model, 'SerialKeyV') ?>

    <?php // echo $form->field($model, 'UserRole') ?>

    <?php // echo $form->field($model, 'Admin') ?>

    <?php // echo $form->field($model, 'referral_code') ?>

    <?php // echo $form->field($model, 'agent_id') ?>

    <?php // echo $form->field($model, 'LastLogin') ?>

    <?php // echo $form->field($model, 'Device') ?>

    <?php // echo $form->field($model, 'SessionID') ?>

    <?php // echo $form->field($model, 'LastLogin2') ?>

    <?php // echo $form->field($model, 'Device2') ?>

    <?php // echo $form->field($model, 'SessionID2') ?>

    <?php // echo $form->field($model, 'WebSession') ?>

    <?php // echo $form->field($model, 'Email') ?>

    <?php // echo $form->field($model, 'Phone') ?>

    <?php // echo $form->field($model, 'IC') ?>

    <?php // echo $form->field($model, 'Address') ?>

    <?php // echo $form->field($model, 'Country') ?>

    <?php // echo $form->field($model, 'Package') ?>

    <?php // echo $form->field($model, 'Package2') ?>

    <?php // echo $form->field($model, 'expiry2') ?>

    <?php // echo $form->field($model, 'last_expiry2') ?>

    <?php // echo $form->field($model, 'Package3') ?>

    <?php // echo $form->field($model, 'expiry3') ?>

    <?php // echo $form->field($model, 'Build') ?>

    <?php // echo $form->field($model, 'Versions') ?>

    <?php // echo $form->field($model, 'DeviceToken') ?>

    <?php // echo $form->field($model, 'dob') ?>

    <?php // echo $form->field($model, 'gender') ?>

    <?php // echo $form->field($model, 'age_group') ?>

    <?php // echo $form->field($model, 'invest_exp') ?>

    <?php // echo $form->field($model, 'special1') ?>

    <?php // echo $form->field($model, 'special2') ?>

    <?php // echo $form->field($model, 'special3') ?>

    <?php // echo $form->field($model, 'years1') ?>

    <?php // echo $form->field($model, 'years2') ?>

    <?php // echo $form->field($model, 'years3') ?>

    <?php // echo $form->field($model, 'sector1') ?>

    <?php // echo $form->field($model, 'sector2') ?>

    <?php // echo $form->field($model, 'sector3') ?>

    <?php // echo $form->field($model, 'sector4') ?>

    <?php // echo $form->field($model, 'hide_profile') ?>

    <?php // echo $form->field($model, 'profile_update') ?>

    <?php // echo $form->field($model, 'profile_status') ?>

    <?php // echo $form->field($model, 'create_date') ?>

    <?php // echo $form->field($model, 'self_create_date') ?>

    <?php // echo $form->field($model, 'payment') ?>

    <?php // echo $form->field($model, 'payment2') ?>

    <?php // echo $form->field($model, 'remark') ?>

    <?php // echo $form->field($model, 'mtp_group') ?>

    <?php // echo $form->field($model, 'auth_key') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
