<?php

namespace app\models;

use Yii;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Users */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="users-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'UserID')->textInput(['readOnly' => !$model->isNewRecord]) ?>
    
    <?= $form->field($model, 'UserPWD')->textInput(['maxlength' => true]) ?>

    <?php if ($model->isNewRecord/* || Yii::$app->session->get('user.role') >= Yii::$app->params['administrator']*/)
        echo $form->field($model, 'showpassword')->checkbox([
           'template' => "<div class=\"col-lg-offset-1 col-lg-3\">{input} {label}</div>\n<div class=\"col-lg-8\">{error}</div>",
        ]) ?>

    <?= $form->field($model, 'UserName')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'UserRole')->textInput() ?>

    <?= $form->field($model, 'Email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'last_expiry2')->widget(
        DatePicker::className(), [
        'name' => 'date',
        'value' => '12-31-2010',
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd'
        ]
    ]);?>

    <?= $form->field($model, 'remark')->textArea(array('rows'=>3)); ?>

    <style>
        div#packageArea {
            padding-bottom: 22px;
        }
    </style>

    <div id="packageArea">
        <table class="table table-bordered" id="dynamicPackageOptions">
            <tr>
                <th>Package</th>
                <th>Expiry Date</th>
                <th>Action</th>
            </tr>
            <?php
            if ( $model->isNewRecord){
            ?>
            <tr>
                <td>
                    <?php
                    $packageList=ArrayHelper::map(Package::find()->all(),'package_id', 'package_id');
                    ?>
                    <select class="form-control"  name="Package[0][name]" id="packageBlock">
                        <?php
                        foreach ($packageList as $key=>$items){
                            ?>
                            <option value="<?=$key?>"><?=$items?></option>
                            <?php
                        }
                        ?>
                    </select>
                </td>
                <td>
                    <input type="text" data-provide="datepicker" placeholder="10/12/2019"  name="Package[0][expire_date]" class="form-control datepicker ">
                </td>
                <td>
                    <button type="button" class="btn btn-danger remove-tr">X</button>
                </td>
            </tr>
            <?php }else{
                $userId=$model->UserID;
                $mappingPackageModel=MappingPackage::find()->where(['user_id'=>$userId])->all();
                if ($mappingPackageModel){
                    foreach ($mappingPackageModel as $keyItemsMapping=>$itemsMappings):
                    ?>
                        <tr>
                            <td>
                                <?php
                                $packageList=ArrayHelper::map(Package::find()->all(),'package_id', 'package_id');
                                ?>
                                <select class="form-control"  name="Package[<?=$keyItemsMapping?>][name]" id="packageBlock">
                                    <?php
                                    foreach ($packageList as $key=>$items){
                                        $select="";
                                        if ($itemsMappings['package_name']==$items){
                                            $select="selected";
                                        }
                                        ?>
                                        <option value="<?=$key?>" <?=$select?>><?=$items?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </td>
                            <td>
                                <input type="text" data-provide="datepicker" placeholder="10/12/2019"  name="Package[<?=$keyItemsMapping?>][expire_date]" class="form-control datepicker" value="<?=date("m/d/Y", strtotime($itemsMappings['expire']))?>">
                            </td>
                            <td>
                                <button type="button" class="btn btn-danger remove-tr">X</button>
                            </td>
                        </tr>
               <?php endforeach; } else{
                ?>
                <tr>
                    <td>
                        <?php
                        $packageList=ArrayHelper::map(Package::find()->all(),'package_id', 'package_id');
                        ?>
                        <select class="form-control"  name="Package[0][name]" id="packageBlock">
                            <?php
                            foreach ($packageList as $key=>$items){
                                ?>
                                <option value="<?=$key?>"><?=$items?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </td>
                    <td>
                        <input type="text" data-provide="datepicker" placeholder="10/12/2019"  name="Package[0][expire_date]" class="form-control datepicker ">
                    </td>
                    <td>
                        <button type="button" class="btn btn-danger remove-tr">X</button>
                    </td>
                </tr>
            <?php }  }?>
        </table>
        <button type="button" name="add" id="add" class="btn btn-success">Add more package</button>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$this->registerJs("jQuery('#users-showpassword').change(function(){jQuery('#users-userpwd').attr('type',this.checked?'text':'password');})");
?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript">
    $( document ).ready(function() {
        var index = $('#dynamicPackageOptions tbody tr').length;
        var packagehtml = $('#packageBlock').html();

        $("#add").click(function(){
            $("#dynamicPackageOptions").append('<tr>' +
                '<td><select class="form-control"  name="Package['+index+'][name]">'+packagehtml+'</select></td>' +
                '<td><input type="text" data-provide="datepicker" placeholder="10/12/2019" name="Package['+ index +'][expire_date]" class="form-control datepicker "></td>' +
                '<td><button type="button" class="btn btn-danger remove-tr">X</button></td></tr>');
            index++;
        });
    });

    $(document).on('click', '.remove-tr', function(){
        $(this).parents('tr').remove();
    });
</script>
