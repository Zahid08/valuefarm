<?php

namespace app\models;

use yii;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Users */

$this->title = $model->UserName;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="users-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php
        //$role = Yii::$app->session->get('user.role');
        $role = Users::findOne(Yii::$app->user->id)->UserRole;
    ?>

    <p>
        <?= Html::a('<< Back', ['index'], ['class' => 'btn btn-primary']) ?>
        <?php
            
            if ($role >= Yii::$app->params['administrator'] || Yii::$app->user->id == $model->agent_id)
                echo Html::a('Update', ['update', 'id' => $model->UserID], ['class' => 'btn btn-primary']);
            
            if ($role >= Yii::$app->params['administrator'])
                echo Html::a('Delete', ['delete', 'id' => $model->UserID], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                ],
            ])
        ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'UserID',
            //'UserID2',
            ['attribute' => 'UserPWD',
             'value' => function ($model) {return substr($model->UserPWD,0,2) . str_pad('*',20 /*strlen($model->UserPWD)-2*/,'*');}
            ],
            //'UserPWD',
            //'UserKey',
            'UserName',
            //'UserExpiryDate',
            //'pre_mco_expiry',
            //'pre_mco_expiry_vf',
            //'last_expiry',
            //'UserLastUpdatedDate',
            //'UserNextRegistrationDate',
            //'SerialKey',
            //'SerialKeyM',
            //'SerialKeyV',
            ['attribute' => 'UserRole',
             'format' => 'raw',
             'value' => $model->userRoleText,
            ],
            //'Admin',
            'referral_code',
            'agent_id',
            'LastLogin',
            //'Device',
            //'SessionID',
            'LastLogin2',
            //'Device2',
            //'SessionID2',
            //'WebSession',
            'Email:email',
            'Phone',
            //'IC',
            //'Address',
            //'Country',
            //'Package',
            //'Package2',
//            ['attribute' => 'Package',
//             'format' => 'raw',
//             'value' => function ($model) {
//                 return $model->Package == '' ? Yii::$app->formatter->nullDisplay : $model->Package . '<br><small>Exp: ' .
//                        (!$model->UserExpiryDate ? Yii::$app->formatter->nullDisplay : $model->UserExpiryDate) . '<small>';
//            }],
//            ['attribute' => 'Package2',
//             'format' => 'raw',
//             'value' => function ($model) {
//                 return $model->Package2 == '' ? Yii::$app->formatter->nullDisplay : $model->Package2 . '<br><small>Exp: ' .
//                        (!$model->expiry2 ? Yii::$app->formatter->nullDisplay : $model->expiry2) . '<small>';
//            }],
            //'expiry2',
            //'last_expiry2',
            //'Package3',
            //'expiry3',
            'Build',
            'Versions:ntext',
            //'DeviceToken',
            //'dob',
            //'gender',
            //'age_group',
            //'invest_exp',
            //'special1',
            //'special2',
            //'special3',
            //'years1',
            //'years2',
            //'years3',
            //'sector1',
            //'sector2',
            //'sector3',
            //'sector4',
            //'hide_profile',
            ///'profile_update',
            //'profile_status',
            'create_date',
            'self_create_date',
            'payment',
            'payment2',
            'remark',
            //'mtp_group',
            //'auth_key',
        ],
        'options' => ['class' => 'table table-striped table-bordered detail-view fix-width'],
    ]) ?>

    <?php
    $mappingPackageModel=MappingPackage::find()->where(['user_id'=>$model->UserID])->all();
    ?>
    <table id="w0" class="table table-striped table-bordered detail-view fix-width"><tbody>
        <?php
        foreach ($mappingPackageModel as $key=>$items){
        ?>
        <tr><th>Package - <?=$key?></th><td><?=$items->package_name?></td></tr>
        <?php }?>
        </tbody>
    </table>

</div>

<style>
    .fix-width > tbody > tr > th {width: 30%;}
</style>
