<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Course */

$this->title = $model->course_name;
$this->params['breadcrumbs'][] = ['label' => 'Courses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="course-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('<< Back', ['index'], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Update', ['update', 'id' => $model->course_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->course_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>

        <?= Html::a(Yii::t('app', 'Batch'), ['batch/index','id' => $model->course_id] , ['class' => 'btn btn-info']); ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'course_id',
            'course_name',
            'description',
            ['attribute' => 'active',
            'value' => function ($model) {
                return $model->active ? "Yes" : "No";
            },
            'contentOptions' => ['style' => $model->active ? 'color:green' :'color:red']],
            //'lastupdate',
        ],
    ]) ?>

<h2>Course Content</h2>


<?= Html::a('Create Course content', ['coursecontent/create', 'id' => $model->course_id], ['class' => 'btn btn-success']) ?>


<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        //'coursecontent_id',
        'chapter_name',
        'description',
        'video_link',
        ['attribute' => 'active',
        'value' => function ($model) {
            return $model->active ? "Yes" : "No";
        },
        'contentOptions' => function ($model, $key, $index, $column) { 
            return $model->active ? ['style' => 'color:green'] : ['style' => 'color:red']; 
        }],
        //'seq',
        //'course_id',

        ['class' => 'yii\grid\ActionColumn',
             
             'contentOptions' => ['class' => 'text-center'],
             'buttons' => [
                'view' => function ($url,$model) {
                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>',['coursecontent/view', 'id' => $model->coursecontent_id]);
                },
                'update' => function ($url,$model) {
                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>',['coursecontent/update', 'id' => $model->coursecontent_id]);
                },
                'delete' => function ($url,$model) {
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>',['coursecontent/delete', 'id' => $model->coursecontent_id],
                        ['data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ]);
                },
            ]],
    ],
]); ?>





    

</div>
