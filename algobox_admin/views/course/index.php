<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Courses';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="course-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Course', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'course_id',
            //'course_name',
            
            ['attribute' => 'course_name', 
             'headerOptions' => ['width' => '200px'],
             'format' => 'raw', 
             'value' => function ($model) {
                return Html::a($model->course_name,['course/view', 'id' => $model->course_id]);
            }],
            
            'description',
            ['attribute' => 'active',
            'filter'=> ['1'=>'Yes','0'=>'No'],
            'value' => function ($model, $key, $index, $column) {return $model->isActive;},
            'contentOptions' => function ($model, $key, $index, $column) { 
               return $model->active == 1 ? ['style' => 'color:green', 'class' => 'text-center'] : ['style' => 'color:red', 'class' => 'text-center']; 
           }],
            'lastupdate',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
