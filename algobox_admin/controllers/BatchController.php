<?php

namespace app\controllers;

use app\models\Course;
use app\models\UsersSearch;
use Yii;
use app\models\Batch;
use app\models\Batchcontent;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * BatchController implements the CRUD actions for Batch model.
 */
class BatchController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }


    /**
     * Lists all Batch models.
     * @return mixed
     */

    public function actionIndex($id)
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Batch::find()->andFilterWhere(['course_id' =>$id]),
        ]);

        $courseModel=Course::findOne($id);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'id' => $id,
            'courseModel' => $courseModel
        ]);
    }

    public function actionUserbatch($id)
    {
        if (isset($_GET['pageSize'])) {
            Yii::$app->session->set('usersPageSize', (int) $_GET['pageSize']);
            unset($_GET['pageSize']);
        }

        $batchModel=\app\models\Batch::findOne($id);
        $assignUserData='';
        if ($batchModel){
            $assignUserData=(array)json_decode($batchModel->assign_users);
        }
        $searchModel = new UsersSearch();
        $dataProvider = $searchModel->searchAssigUser(Yii::$app->request->queryParams,$assignUserData);

        return $this->render('userbatch', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Displays a single Batch model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Batchcontent::find()->andFilterWhere(['batch_id' =>$id]),
            
        ]);

        return $this->render('view', [
            'model' => $this->findModel($id), 'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Batch model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Batch();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->batch_id]);
        }

        $model->date = date("Y-m-d H:i:s",time());
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Batch model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->batch_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionUpdateAssignUsers($id='')
    {
        $post = Yii::$app->request->post();

        if ($post){
            $batchId=$post['batchIdValue'];
            $selectionsData=$post['selection'];
            $gettingBatchModel=Batch::findOne($batchId);
            $gettingBatchModel->assign_users=json_encode($selectionsData);
                if ($gettingBatchModel->save()){
                    echo 1;
                    exit();
                }else{
                    echo 2;
                    exit();
                }
        }
    }

    /**
     * Deletes an existing Batch model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id,$courseId='')
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index','id'=>$courseId]);
    }

    /**
     * Finds the Batch model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Batch the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Batch::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
